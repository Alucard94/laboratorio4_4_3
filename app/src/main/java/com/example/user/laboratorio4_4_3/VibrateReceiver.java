package com.example.user.laboratorio4_4_3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

/**
 * Created by User on 24/01/2016.
 */
public class VibrateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent){
        Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrator.vibrate(500);
    }
}
